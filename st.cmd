# Startup for Tgt-TSS1080

require essioc
require s7plc
require modbus
require calc

# Load standard IOC startup scripts
iocshLoad("$(essioc_DIR)/common_config.iocsh")

# Register our db directory
epicsEnvSet(EPICS_DB_INCLUDE_PATH, "$(E3_CMD_TOP)/db:$(EPICS_DB_INCLUDE_PATH=.)")

# Load PLC specific startup script
iocshLoad("$(E3_CMD_TOP)/iocsh/plcfactory.iocsh", "MODVERSION=$(IOCVERSION=)")
